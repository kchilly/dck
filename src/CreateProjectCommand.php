<?php

namespace Csoft\DockerTool;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateProjectCommand extends Command
{
    const ARG_DIRECTORY = 'directory';
    const ARG_DATABASE  = 'database';
    const ARG_CONFIG    = 'config';

    const DATABASE_MYSQL      = 'mysql';
    const DATABASE_PSQL       = 'psql';

    const DEFAULT_CONFIG_PATH = 'project';

    private $availableDatabase = [
        'mysql'      => self::DATABASE_MYSQL,
        'psql'       => self::DATABASE_PSQL,
        'pgsql'      => self::DATABASE_PSQL,
        'postgres'   => self::DATABASE_PSQL,
        'postgresql' => self::DATABASE_PSQL,
    ];

    protected function configure()
    {
        $this->setName('create')
            ->setDescription('Creates a new docker project.')
            ->setHelp('This command allows you to create a new docker project...')
        ;

        $this->setDefinition(
            new InputDefinition([
                new InputArgument(self::ARG_DIRECTORY, InputArgument::REQUIRED, 'the new docker project folder path'),
                new InputOption(self::ARG_DIRECTORY, 'd', InputArgument::REQUIRED, 'the new docker project folder path'),
                new InputOption(self::ARG_DATABASE, 's', InputArgument::OPTIONAL, 'the database engine type', self::DATABASE_MYSQL),
                new InputOption(self::ARG_CONFIG, 'c', InputArgument::OPTIONAL, 'the config folder path', self::DEFAULT_CONFIG_PATH),
            ])
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!array_key_exists($input->getOption(self::ARG_DATABASE), $this->availableDatabase)) {
            throw new \InvalidArgumentException(
                'Illegal database type. "' . self::ARG_DATABASE
                . '" argument needs to be one of these: ' . implode(', ', array_keys($this->availableDatabase))
            );
        }

        if (!is_dir($input->getOption(self::ARG_DIRECTORY))) {
            throw new \ErrorException('The given "' . self::ARG_DIRECTORY . '" is not a valid path.');
        }
    }
}
