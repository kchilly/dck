<?php

namespace Csoft\DockerTool;


use Symfony\Component\Process\Process;

class DockerComposeService
{
    /** @var string */
    private $environment;

    public function __construct(string $environment)
    {
        $this->environment = $environment;
    }

    public function getArgumentsListFromArguments(string $arguments): array
    {
        return explode(' ', $arguments);
    }

    public function getFirstFromArgumentsList(array $argumentsList): string
    {
        return array_shift($argumentsList);
    }

    public function buildProcess(string $type, array $argumentsList): Process
    {
        switch ($type) {
            // If built in docker-compose command
            case 'build':
            case 'bundle':
            case 'config':
            case 'create':
            case 'down':
            case 'events':
            case 'exec':
            case 'images':
            case 'kill':
            case 'logs':
            case 'pause':
            case 'port':
            case 'ps':
            case 'pull':
            case 'push':
            case 'restart':
            case 'rm':
            case 'run':
            case 'scale':
            case 'start':
            case 'stop':
            case 'top':
            case 'unpause':
            case 'up':
                return new Process(array_merge($this->getDockerComposeBaseCommandList(), $argumentsList));
                break;

            // For reaching the mysql console
            case 'mysql':
                return new Process(array_merge($this->getDockerComposeBaseCommandList(), ['exec', 'database', 'bash', '-c', '"mysql -u ${DATABASE_USER} -p${DATABASE_PASSWORD}"']));
                break;

            // For reaching the psql console
            case 'psql':
                return new Process(array_merge($this->getDockerComposeBaseCommandList(), ['exec', 'database', 'bash', '-c', '"PGPASSWORD=\'${DATABASE_PASSWORD}\' psql -U ${DATABASE_USER} ${DATABASE_NAME}"']));
                break;

            // For running commands on the main 'web' container
            default:
                return new Process(array_merge($this->getDockerComposeBaseCommandList(), ['exec', 'web'], $argumentsList));
        }
    }

    private function getDockerComposeBaseCommandList(): array
    {
        return ['docker-compose', '-f', 'docker-compose.yml', '-f', 'docker-compose-' . $this->environment . '.yml'];
    }
}
