<?php

namespace Csoft\DockerTool;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class DockerComposeCommand extends Command
{
    /** @var DockerComposeService */
    private $dockerComposeService;

    public function __construct(DockerComposeService $dockerComposeService, string $name = null)
    {
        $this->dockerComposeService = $dockerComposeService;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('compose')
            ->setDescription('Runs docker-compose on dcktool docker project.')
            ->setHelp('This command allows you to use special docker-compose on a dcktool docker project...')
        ;

        $this->addOption('cmd', 'c', InputOption::VALUE_REQUIRED, 'docker-compose command or running commands in the web container');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argumentsList = $this->dockerComposeService->getArgumentsListFromArguments($input->getOption('cmd'));
        $type = $this->dockerComposeService->getFirstFromArgumentsList($argumentsList);

        $process = $this->dockerComposeService->buildProcess($type, $argumentsList);
        $output->writeln('<info>Actual command to run: ' . $process->getCommandLine() . '</info>');

        $process->setTimeout(1200);

        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                fwrite(STDERR, $buffer);
            } else {
                echo $buffer;
            }
        });

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            exit($process->getExitCode());
        }
    }
}
