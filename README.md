# Docker tool
>This tool is for generate docker projects based on git repository with php and mysql/postgresql.  
## Installation
````bash
composer global require csoft/docker-tool
````
## Usage
### Create a docker project
#### Command
`vendor/bin/docker-tool create`
#### Parameters
| short arg | long arg    | value                     | description                        |
|-----------|-------------|---------------------------|------------------------------------|
| -d        | --directory | [directory path]          | the new docker project folder path |
| -s        | --database  | [mysql, psql, postgresql] | the database engine type           |
| -c        | --config    | [config folder path]      | the config folder path             |


